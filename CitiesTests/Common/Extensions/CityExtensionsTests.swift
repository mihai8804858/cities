@testable import Cities
import XCTest

final class CityExtensionsTests: XCTestCase {
    func testCityCorrectlyReturnsTitle() {
        let city = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let title = city.title
        XCTAssertEqual(title, "New York, US")
    }

    func testCityCorrectlyReturnsCoordinatesTitle() {
        let city = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 456.1, latitude: 789.2)
        )
        let title = city.coordinatesTitle
        XCTAssertEqual(title, "Lon: 456.1, Lat: 789.2")
    }

    func testComparisonWhenFirstCityNameIsSmaller() {
        let city1 = City(
            countryCode: "NL",
            name: "Amsterdam",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let city2 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        XCTAssert(city1 < city2)
    }

    func testComparisonWhenFirstCityNameIsBigger() {
        let city1 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let city2 = City(
            countryCode: "NL",
            name: "Amsterdam",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        XCTAssertFalse(city1 < city2)
    }

    func testComparisonWhenCityNamesAreEqualAndFirstCountryIsSmaller() {
        let city1 = City(
            countryCode: "NL",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let city2 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        XCTAssert(city1 < city2)
    }

    func testComparisonWhenCityNamesAreEqualAndFirstCountryIsBigger() {
        let city1 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let city2 = City(
            countryCode: "NL",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        XCTAssertFalse(city1 < city2)
    }

    func testComparisonWhenCityNamesAndCountryAreEqual() {
        let city1 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        let city2 = City(
            countryCode: "US",
            name: "New York",
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
        XCTAssertFalse(city1 < city2)
    }
}
