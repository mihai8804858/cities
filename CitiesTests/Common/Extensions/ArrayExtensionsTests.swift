@testable import Cities
import XCTest

final class ArrayExtensionsTests: XCTestCase {
    func testQuickSortWhenArrayIsAlreadySorted() {
        let collection = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        var result = collection
        result.quickSort()
        XCTAssertEqual(result, collection)
    }

    func testQuickSortWhenArrayIsUnsorted() {
        let collection = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].shuffled()
        var result = collection
        result.quickSort()
        XCTAssertEqual(result, collection.sorted())
    }
}
