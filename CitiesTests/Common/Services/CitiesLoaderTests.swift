@testable import Cities
import Foundation
import XCTest

final class CitiesLoaderTests: XCTestCase {
    func testLoaderFailsWithResourceNotFoundErrorWhenURLIsMissing() {
        // GIVEN
        let bundle = MockBundle()
        bundle.urlForResourceStub = nil
        let loader = CitiesLoader(bundle: bundle)
        let resource = FileResource(fileName: "cities", fileType: "json")

        // WHEN
        var loadingResult: CitiesLoadingResult?
        let expectation = self.expectation(description: "Cities loading expectation")
        loader.loadCities(from: resource) { result in
            loadingResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)

        // THEN
        switch loadingResult {
        case .none:
            XCTFail("Expected to fail")
        case .success:
            XCTFail("Expected to fail")
        case .failure(let error):
            XCTAssertEqual(error, .resourceNotFound(message: "Resource \(resource.fullName) could not be found"))
        }
    }

    func testLoaderFailsWhenDataCantBeLoaded() {
        // GIVEN
        let bundle = MockBundle()
        bundle.urlForResourceStub = URL(string: "/some/bad/url")
        let loader = CitiesLoader(bundle: bundle)
        let resource = FileResource(fileName: "cities", fileType: "json")

        // WHEN
        var loadingResult: CitiesLoadingResult?
        let expectation = self.expectation(description: "Cities loading expectation")
        loader.loadCities(from: resource) { result in
            loadingResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)

        // THEN
        switch loadingResult {
        case .none:
            XCTFail("Expected to fail")
        case .success:
            XCTFail("Expected to fail")
        case .failure(let error):
            guard case .underlying = error else {
                return XCTFail("Expected to fail with underlying error")
            }
        }
    }

    func testLoaderFailsWithDecodingErrorWhenDataCantBeParsed() {
        // GIVEN
        let bundle = Bundle(for: CitiesLoaderTests.self)
        let loader = CitiesLoader(bundle: bundle)
        let resource = FileResource(fileName: "bad_cities", fileType: "json")

        // WHEN
        var loadingResult: CitiesLoadingResult?
        let expectation = self.expectation(description: "Cities loading expectation")
        loader.loadCities(from: resource) { result in
            loadingResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)

        // THEN
        switch loadingResult {
        case .none:
            XCTFail("Expected to fail")
        case .success:
            XCTFail("Expected to fail")
        case .failure(let error):
            guard case .underlying(let underlyingError) = error else {
                return XCTFail("Expected to fail with underlying error")
            }
            XCTAssert(underlyingError is DecodingError)
        }
    }

    func testLoaderSucceedsWhenDataCanBeParsed() {
        // GIVEN
        let bundle = Bundle(for: CitiesLoaderTests.self)
        let loader = CitiesLoader(bundle: bundle)
        let resource = FileResource(fileName: "good_cities", fileType: "json")
        let expectedCities = [
            City(countryCode: "UA", name: "Hurzuf", id: 707860,
                 coordinates: City.Coordinates(longitude: 34.283333, latitude: 44.549999)),
            City(countryCode: "RU", name: "Novinki", id: 519188,
                 coordinates: City.Coordinates(longitude: 37.666668, latitude: 55.683334)),
            City(countryCode: "NP", name: "Gorkhā", id: 1283378,
                 coordinates: City.Coordinates(longitude: 84.633331, latitude: 28))
        ]

        // WHEN
        var loadingResult: CitiesLoadingResult?
        let expectation = self.expectation(description: "Cities loading expectation")
        loader.loadCities(from: resource) { result in
            loadingResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)

        // THEN
        switch loadingResult {
        case .none:
            XCTFail("Expected to fail")
        case .failure(let error):
            XCTFail(error.localizedDescription)
        case .success(let cities):
            XCTAssertEqual(cities, expectedCities)
        }
    }
}
