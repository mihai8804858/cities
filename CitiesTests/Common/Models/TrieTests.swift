@testable import Cities
import XCTest

final class TrieTests: XCTestCase {
    private var trie: Trie<City>?

    private func createCity(name: String, country: String) -> City {
        return City(
            countryCode: country,
            name: name,
            id: 123,
            coordinates: .init(longitude: 123, latitude: 123)
        )
    }

    private func search(for name: String) -> Set<City> {
        return Set(trie?.values(startingWith: name) ?? [])
    }

    override func setUp() {
        super.setUp()
        let trie = Trie<City>()
        trie.insert(createCity(name: "Alabama", country: "US"))
        trie.insert(createCity(name: "Alabama", country: "NL"))
        trie.insert(createCity(name: "Albuquerque", country: "US"))
        trie.insert(createCity(name: "Anaheim", country: "US"))
        trie.insert(createCity(name: "Arizona", country: "US"))
        trie.insert(createCity(name: "Sydney", country: "AU"))
        self.trie = trie
    }

    func testTrieLookupWithMultipleResults() {
        let results = search(for: "al")
        XCTAssertEqual(results, [
            createCity(name: "Alabama", country: "US"),
            createCity(name: "Alabama", country: "NL"),
            createCity(name: "Albuquerque", country: "US")
        ])
    }

    func testTrieLookupWithSingleResult() {
        let results = search(for: "syd")
        XCTAssertEqual(results, [
            createCity(name: "Sydney", country: "AU")
        ])
    }

    func testTrieLookupWithNoResults() {
        let results = search(for: "au")
        XCTAssert(results.isEmpty)
    }
}
