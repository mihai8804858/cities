@testable import Cities
import Foundation

final class MockBundle: BundleType {
    var urlForResourceCallParameters: (String?, String?)?
    var urlForResourceStub: URL?

    var urlForResourceCalled: Bool {
        return urlForResourceCallParameters != nil
    }

    func url(forResource name: String?, withExtension ext: String?) -> URL? {
        urlForResourceCallParameters = (name, ext)
        return urlForResourceStub
    }
}
