import Foundation

enum CitiesLoadingError: Swift.Error, Equatable {
    case resourceNotFound(message: String)
    case underlying(error: Error)

    static func == (lhs: CitiesLoadingError, rhs: CitiesLoadingError) -> Bool {
        switch (lhs, rhs) {
        case (.resourceNotFound(let lhsMessage), .resourceNotFound(let rhsMessage)):
            return lhsMessage == rhsMessage
        case (.underlying(let lhsError), .underlying(let rhsError)):
            return lhsError.localizedDescription == rhsError.localizedDescription
        default:
            return false
        }
    }
}

typealias CitiesLoadingResult = Result<[City], CitiesLoadingError>

struct FileResource {
    let fileName: String
    let fileType: String

    var fullName: String {
        return "\(fileName).\(fileType)"
    }
}

/// Loads data from a bundled file and decodes it.
protocol CitiesLoading {
    /// Loads data from `resource` and decodes it.
    /// - Parameters:
    ///   - resource: File resource (file name and extension)
    ///   - callbackQueue: Queue on which completion block will be called (`.main` if none is provided)
    ///   - completion: Completion block to be called
    func loadCities(from resource: FileResource,
                    callbackQueue: DispatchQueue?,
                    completion: @escaping (CitiesLoadingResult) -> Void)
}

extension CitiesLoading {
    func loadCities(from resource: FileResource, completion: @escaping (CitiesLoadingResult) -> Void) {
        loadCities(from: resource, callbackQueue: nil, completion: completion)
    }
}

struct CitiesLoader: CitiesLoading {
    private let bundle: BundleType
    private let workQueue = DispatchQueue(label: "com.mseremet.Cities.loader_queue", qos: .utility)

    init(bundle: BundleType) {
        self.bundle = bundle
    }

    func loadCities(from resource: FileResource,
                    callbackQueue: DispatchQueue?,
                    completion: @escaping (CitiesLoadingResult) -> Void) {
        workQueue.async {
            guard let url = self.bundle.url(forResource: resource.fileName, withExtension: resource.fileType) else {
                return self.complete(
                    with: .failure(.resourceNotFound(message: Constants.Generic.resourceNotFound(resource.fullName))),
                    completion: completion,
                    on: callbackQueue
                )
            }
            do {
                let data = try Data(contentsOf: url)
                let cities = try JSONDecoder().decode([City].self, from: data)
                self.complete(with: .success(cities), completion: completion, on: callbackQueue)
            } catch {
                self.complete(with: .failure(.underlying(error: error)), completion: completion, on: callbackQueue)
            }
        }
    }

    private func complete(with result: CitiesLoadingResult,
                          completion: @escaping (CitiesLoadingResult) -> Void,
                          on queue: DispatchQueue?) {
        (queue ?? .main).async { completion(result) }
    }
}
