import Foundation

/// Debounces an action using provided `interval`.
protocol Debouncing {
    /// Time number of seconds that should pass without enqueuing new action or cancelling current one in order for action to be executed.
    var interval: TimeInterval { get }

    /// Enque an action to be executed. If new action will be enqueued before `interval` seconds, current one will be overriden.
    /// - Parameter action: Action to be debounced
    func debounce(_ action: @escaping () -> Void)

    /// Cancels current action.
    func cancel()
}

final class Debouncer: Debouncing {
    let interval: TimeInterval
    private var timer: Timer?

    init(interval: TimeInterval) {
        self.interval = interval
    }

    deinit {
        cancel()
    }

    func debounce(_ action: @escaping () -> Void) {
        cancel()
        timer = .scheduledTimer(withTimeInterval: interval, repeats: false) { _ in action() }
    }

    func cancel() {
        timer?.invalidate()
        timer = nil
    }
}
