import Foundation

struct City: Hashable, Codable {
    struct Coordinates: Hashable, Codable {
        let longitude: Double
        let latitude: Double

        enum CodingKeys: String, CodingKey {
            case longitude = "lon"
            case latitude = "lat"
        }
    }

    let countryCode: String
    let name: String
    let id: Int
    let coordinates: Coordinates

    enum CodingKeys: String, CodingKey {
        case countryCode = "country"
        case name
        case id = "_id"
        case coordinates = "coord"
    }
}
