import Foundation

protocol TrieRepresentable: Equatable {
    associatedtype Representation: Collection

    var trieRepresentation: Representation { get }
}

/// A tree like representation of a set of collections (e.g. strings). This can be used for fast search using prefixes (e.g. autocomplete systems).
/// Trie is composed of root nodes and children nodes. Each node is marked as `.nonTerminating` (the collection is not finished) and `.terminating` (a collection finishes here).
/// Terminating nodes have a payload for more complex data storage.
/// Current implementation supports insert and search, orher action like remove can be added when needed.
final class Trie<Value: TrieRepresentable> where Value.Representation.Element: Hashable {
    final class Node<Value: TrieRepresentable> where Value.Representation.Element: Hashable {
        enum Termination<Value> {
            case nonTerminating
            case terminating(values: [Value])

            var isTerminating: Bool {
                switch self {
                case .nonTerminating: return false
                case .terminating: return true
                }
            }
        }

        weak var parent: Node?

        var key: Value.Representation.Element?
        var children: [Value.Representation.Element: Node] = [:]
        var termination: Termination<Value> = .nonTerminating

        var isTerminating: Bool {
            return termination.isTerminating
        }

        init(key: Value.Representation.Element?, parent: Node?) {
            self.key = key
            self.parent = parent
        }

        func remove(value: Value) {
            switch termination {
            case .nonTerminating:
                return
            case .terminating(var values):
                values.removeAll { $0 == value }
                if values.isEmpty {
                    termination = .nonTerminating
                } else {
                    termination = .terminating(values: values)
                }
            }
        }
    }

    private let root = Node<Value>(key: nil, parent: nil)

    init() {}

    /// Insert a collection in the tree and add paylod to terminating node.
    /// - Parameters:
    ///   - collection: Nodes path to traverse until terminating node.
    ///   - value: Payload to be added to terminating node.
    func insert(_ value: Value) {
        var current = root
        for element in value.trieRepresentation {
            if let next = current.children[element] {
                current = next
            } else {
                let newNode = Node(key: element, parent: current)
                current.children[element] = newNode
                current = newNode
            }
        }
        switch current.termination {
        case .nonTerminating:
            current.termination = .terminating(values: [value])
        case .terminating(let values):
            current.termination = .terminating(values: values + [value])
        }
    }
}

extension Trie where Value.Representation: RangeReplaceableCollection {
    /// Searches the tree for all collections and payloads guven a `prefix`.
    /// - Parameter prefix: The collection prefix to search against.
    func values(startingWith representation: Value.Representation) -> [Value] {
        var current = root
        for element in representation {
            guard let child = current.children[element] else { return [] }
            current = child
        }

        return values(startingWith: representation, after: current)
    }

    private func values(startingWith representation: Value.Representation, after node: Node<Value>) -> [Value] {
        var results: [Value] = []
        switch node.termination {
        case .nonTerminating: break
        case .terminating(let values): results.append(contentsOf: values)
        }
        for child in node.children.values {
            guard let key = child.key else { continue }
            var representation = representation
            representation.append(key)
            results.append(contentsOf: values(startingWith: representation, after: child))
        }

        return results
    }
}
