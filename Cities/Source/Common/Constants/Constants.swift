enum Constants {
    enum Generic {
        static func resourceNotFound(_ resource: String) -> String {
            return "Resource \(resource) could not be found"
        }

        static let retry = "Retry"
        static let oops = "Oops!"
        static let somethingWentWrong = "Something went wrong!"
    }

    enum CitiesList {
        static func cityTitle(name: String, country: String) -> String {
            return "\(name), \(country)"
        }

        static func cityCoordinatesTitle(longitude: Double, latitude: Double) -> String {
            return "Lon: \(longitude), Lat: \(latitude)"
        }

        static func noResults(for query: String) -> String {
            return "There are no results for '\(query)'."
        }

        static let citiesResourceFileName = "cities"
        static let citiesResourceFileType = "json"
        static let citiesTitle = "Cities"
        static let noCities = "There are no cities"
    }

    enum Map {
        static let title = "Map"
        static let radius: Double = 100_000
    }
}
