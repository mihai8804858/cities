import UIKit

/// Instantiates a view controller from storyboard by controller identifier and casts it to the appropriate type.
protocol StoryboardInstantiable {
    /// File name of the storyboard where view controller is defined.
    static var storyboardName: String { get }

    /// View controller identifier in storyboard. Default to class name of view controller.
    static var storyboardID: String { get }

    /// Bundle where storyboard is defined. Default to `nil`.
    static var bundle: Bundle? { get }
}

extension StoryboardInstantiable {
    static var bundle: Bundle? {
        return nil
    }

    static var storyboardID: String {
        return String(describing: self)
    }

    /// Instantiates a view controller from storyboard by controller identifier and casts it to the appropriate type.
    static func instantiate() -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: bundle)
        return storyboard.instantiateViewController(withIdentifier: storyboardID) as! Self
    }
}
