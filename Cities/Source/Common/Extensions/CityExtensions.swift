import MapKit

extension City {
    var title: String {
        return Constants.CitiesList.cityTitle(
            name: name,
            country: countryCode
        )
    }

    var coordinatesTitle: String {
        return Constants.CitiesList.cityCoordinatesTitle(
            longitude: coordinates.longitude,
            latitude: coordinates.latitude
        )
    }

    var locationCoordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude
        )
    }
}

extension City: Comparable {
    /// Compares two cities by name and country code (case insensitive). If name of cities is same, then country code will be used.
    /// - Parameters:
    ///   - lhs: First city to compare
    ///   - rhs: Second city to compare
    static func < (lhs: City, rhs: City) -> Bool {
        switch lhs.name.compare(rhs.name, options: .caseInsensitive) {
        case .orderedAscending:
            return true
        case .orderedDescending:
            return false
        case .orderedSame:
            return lhs.countryCode.compare(rhs.countryCode, options: .caseInsensitive) == .orderedAscending
        }
    }
}

extension City: TrieRepresentable {
    var trieRepresentation: String {
        return name.lowercased()
    }
}
