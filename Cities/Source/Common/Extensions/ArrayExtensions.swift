import Foundation

extension Array where Element: Comparable {
    /// Sorts array in place using quick sort argorithm and `Hoare` partition type.
    mutating func quickSort() {
        quickSort(low: 0, high: count - 1)
    }

    private mutating func quickSort(low: Int, high: Int) {
        if low < high {
            let p = partition(low: low, high: high)
            quickSort(low: low, high: p)
            quickSort(low: p + 1, high: high)
        }
    }

    private mutating func partition(low: Int, high: Int) -> Int {
        let pivot = self[low]
        var i = low - 1
        var j = high + 1
        while true {
            repeat { j -= 1 } while self[j] > pivot
            repeat { i += 1 } while self[i] < pivot
            if i < j {
                swapAt(i, j)
            } else {
                return j
            }
        }
    }
}
