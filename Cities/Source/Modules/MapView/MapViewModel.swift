import UIKit
import MapKit

final class MapViewModel: NSObject, MapViewOutput {
    private weak var view: MapViewInput?
    private let city: City

    init(view: MapViewInput?, city: City) {
        self.view = view
        self.city = city
    }

    func viewDidLoad() {
        addAnnotation()
        centerMap()
    }
}

private extension MapViewModel {
    func centerMap() {
        view?.centerMap(on: city.locationCoordinate, radius: Constants.Map.radius)
    }

    func addAnnotation() {
        view?.addAnnotation(CityAnnotation(city: city))
    }
}
