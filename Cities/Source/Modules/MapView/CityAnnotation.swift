import MapKit

final class CityAnnotation: NSObject, MKAnnotation {
    private let city: City

    init(city: City) {
        self.city = city
    }

    var coordinate: CLLocationCoordinate2D {
        return city.locationCoordinate
    }

    var title: String? {
        return city.title
    }
}
