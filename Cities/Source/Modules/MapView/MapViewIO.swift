import UIKit
import MapKit

protocol MapViewInput: AnyObject {
    func centerMap(on location: CLLocationCoordinate2D, radius: CLLocationDistance)
    func addAnnotation(_ annotation: MKAnnotation)
}

protocol MapViewOutput: AnyObject {
    func viewDidLoad()
}
