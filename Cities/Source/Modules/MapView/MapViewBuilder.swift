import UIKit

struct MapViewBuilder {
    func build(city: City) -> UIViewController {
        let view = MapViewController.instantiate()
        let viewModel = MapViewModel(view: view, city: city)
        view.viewModel = viewModel

        return view
    }
}
