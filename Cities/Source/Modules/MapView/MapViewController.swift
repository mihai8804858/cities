import UIKit
import MapKit

final class MapViewController: UIViewController, StoryboardInstantiable, MapViewInput {
    static let storyboardName = "MapView"

    @IBOutlet private weak var mapView: MKMapView!

    var viewModel: MapViewOutput?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Constants.Map.title
        viewModel?.viewDidLoad()
    }

    func centerMap(on location: CLLocationCoordinate2D, radius: CLLocationDistance) {
        DispatchQueue.main.async {
            let coordinateRegion = MKCoordinateRegion(
                center: location,
                latitudinalMeters: radius,
                longitudinalMeters: radius
            )
            self.mapView.setRegion(coordinateRegion, animated: false)
        }
    }

    func addAnnotation(_ annotation: MKAnnotation) {
        DispatchQueue.main.async {
            self.mapView.addAnnotation(annotation)
        }
    }
}
