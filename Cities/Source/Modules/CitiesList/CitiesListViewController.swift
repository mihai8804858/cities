import UIKit

final class CitiesListViewController: UIViewController, StoryboardInstantiable, CitiesListViewInput {
    static let storyboardName = "CitiesList"

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var noResultsLabel: UILabel!
    @IBOutlet private weak var searchBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var searchBarTopConstraint: NSLayoutConstraint!

    var viewModel: CitiesListViewOutput?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        configureKeyboard()
        configureTableView()
        configureSearchBar()
        viewModel?.viewDidLoad()
    }

    func showLoadingActivity() {
        DispatchQueue.main.async {
            self.tableView.isHidden = true
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
    }

    func hideLoadingActivity() {
        DispatchQueue.main.async {
            self.tableView.isHidden = false
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }

    func showSearchBar() {
        DispatchQueue.main.async {
            self.searchBar.isHidden = false
            self.searchBarHeightConstraint.constant = 44
            self.searchBarTopConstraint.constant = 10
        }
    }

    func hideSearchBar() {
        DispatchQueue.main.async {
            self.searchBar.isHidden = true
            self.searchBarHeightConstraint.constant = 0
            self.searchBarTopConstraint.constant = 0
        }
    }

    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.setContentOffset(.zero, animated: false)
        }
    }

    func showNoResults(message: String) {
        DispatchQueue.main.async {
            self.noResultsLabel.text = message
            self.noResultsLabel.isHidden = false
            self.tableView.isHidden = true
        }
    }

    func hideNoResults() {
        DispatchQueue.main.async {
            self.noResultsLabel.text = nil
            self.noResultsLabel.isHidden = true
            self.tableView.isHidden = false
        }
    }

    func showAlert(title: String, message: String, actions: [UIAlertAction]) {
        DispatchQueue.main.async {
            let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
            actions.forEach(controller.addAction)
            self.present(controller, animated: true)
        }
    }

    func showMapView(forCity city: City) {
        DispatchQueue.main.async {
            let mapView = MapViewBuilder().build(city: city)
            if let navigationController = self.navigationController {
                navigationController.pushViewController(mapView, animated: true)
            } else {
                self.present(mapView, animated: true)
            }
        }
    }
}

private extension CitiesListViewController {
    func configureView() {
        title = Constants.CitiesList.citiesTitle
        noResultsLabel.text = nil
        noResultsLabel.isHidden = true
    }
    
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }

    func configureSearchBar() {
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
    }

    func configureKeyboard() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }

    @objc func dismissKeyboard() {
        searchBar.resignFirstResponder()
    }
}

extension CitiesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfCities() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        guard let city = viewModel?.city(at: indexPath) else { return cell }
        cell.textLabel?.text = city.title
        cell.detailTextLabel?.text = city.coordinatesTitle

        return cell
    }
}

extension CitiesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel?.didSelectCity(at: indexPath)
    }
}

extension CitiesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel?.searchTextDidChange(searchText)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
}
