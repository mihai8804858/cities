import UIKit

protocol CitiesListViewInput: AnyObject {
    func showLoadingActivity()
    func hideLoadingActivity()
    func showSearchBar()
    func hideSearchBar()
    func showNoResults(message: String)
    func hideNoResults()
    func reloadData()
    func showAlert(title: String, message: String, actions: [UIAlertAction])
    func showMapView(forCity city: City)
}

protocol CitiesListViewOutput: AnyObject {
    func viewDidLoad()
    func numberOfCities() -> Int
    func city(at indexPath: IndexPath) -> City?
    func didSelectCity(at indexPath: IndexPath)
    func searchTextDidChange(_ searchText: String)
}
