import UIKit

final class CitiesListViewModel: NSObject, CitiesListViewOutput {
    private weak var view: CitiesListViewInput?

    private let resource = FileResource(fileName: Constants.CitiesList.citiesResourceFileName,
                                        fileType: Constants.CitiesList.citiesResourceFileType)
    private let loader: CitiesLoading = CitiesLoader(bundle: Bundle.main)
    private let queryDebouncer: Debouncing = Debouncer(interval: 0.3)
    private let workQueue = DispatchQueue(label: "com.mseremet.Cities.work_queue", qos: .utility)
    private let trie = Trie<City>()

    private var allCities: [City] = []
    private var filteredCities: [City] = []

    init(view: CitiesListViewInput?) {
        self.view = view
    }

    func viewDidLoad() {
        loadCities()
    }

    func numberOfCities() -> Int {
        return filteredCities.count
    }

    func city(at indexPath: IndexPath) -> City? {
        guard 0..<numberOfCities() ~= indexPath.row else { return nil }
        return filteredCities[indexPath.row]
    }

    func didSelectCity(at indexPath: IndexPath) {
        guard let city = self.city(at: indexPath) else { return }
        view?.showMapView(forCity: city)
    }

    func searchTextDidChange(_ searchText: String) {
        if searchText.isEmpty {
            handleFilteredCities(allCities, searchQuery: searchText)
        } else {
            queryDebouncer.debounce { [weak self] in
                self?.workQueue.async { [weak self] in
                    guard let self = self else { return }
                    let filteredCities = self.trie.values(startingWith: searchText.lowercased())
                    self.handleFilteredCities(filteredCities, searchQuery: searchText)
                }
            }
        }
    }
}

private extension CitiesListViewModel {
    func loadCities() {
        view?.hideSearchBar()
        view?.showLoadingActivity()
        loader.loadCities(from: resource, callbackQueue: .main) { [weak self] result in
            switch result {
            case .success(let cities):
                self?.handleCities(cities) {
                    self?.view?.hideLoadingActivity()
                    self?.view?.showSearchBar()
                }
            case .failure(let error):
                self?.view?.hideLoadingActivity()
                self?.view?.showSearchBar()
                self?.handleError(error) { self?.loadCities() }
            }
        }
    }

    func handleCities(_ cities: [City], completion: @escaping () -> Void) {
        workQueue.async { [weak self] in
            guard let self = self else { return }
            self.allCities = cities
            self.allCities.quickSort()
            self.buildTrie(from: self.allCities)
            self.handleFilteredCities(self.allCities, searchQuery: nil)
            completion()
        }
    }

    func buildTrie(from cities: [City]) {
        cities.forEach(trie.insert)
    }

    func handleError(_ error: CitiesLoadingError, onRetry: @escaping () -> Void) {
        let retry = UIAlertAction(title: Constants.Generic.retry, style: .cancel, handler: { _ in onRetry() })
        view?.showAlert(title: Constants.Generic.oops, message: Constants.Generic.somethingWentWrong, actions: [retry])
    }

    func handleFilteredCities(_ cities: [City], searchQuery: String?) {
        filteredCities = cities
        view?.reloadData()
        if filteredCities.isEmpty {
            let message = searchQuery.map(Constants.CitiesList.noResults) ?? Constants.CitiesList.noCities
            view?.showNoResults(message: message)
        } else {
            view?.hideNoResults()
        }
    }
}
