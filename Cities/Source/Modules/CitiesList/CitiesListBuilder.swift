import UIKit

struct CitiesListBuilder {
    func build() -> UIViewController {
        let view = CitiesListViewController.instantiate()
        let viewModel = CitiesListViewModel(view: view)
        view.viewModel = viewModel

        return view
    }
}
